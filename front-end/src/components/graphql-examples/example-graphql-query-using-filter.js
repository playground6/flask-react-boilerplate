//this is an example of graphql query using filter option - filter by ID
import React, { Component } from 'react';
import ApolloClient from "apollo-boost";
import { Query, ApolloProvider } from "react-apollo";
import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "http://localhost:4000"
});

const GET_EMPLOYER_FILTER = gql`
 query employer{
   employer(where: {id: "ck8pjsp9quk3g093453k12o9e"}) {
    id
    name
    email
    createdAt
    updatedAt
   }
  }
`;

class GraphqlQueryFilter extends Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <div className="App">
                    <Query query={GET_EMPLOYER_FILTER}>
                        {/* this is called as a render prop function */}
                        { /* data contains the result of the above query */}
                        {({ loading, data }) => {
                            if (loading) return 'Loading...'
                            { /* grab the result of employer query result from data*/ }
                            const { employer } = data;
                            return <h1>{employer.name}</h1>
                        }}
                    </Query>
                </div>
            </ApolloProvider>
        );
    }
}

export default GraphqlQueryFilter;
