import React from 'react'
import { useForm } from "react-hook-form";

const ReactHookForm = () => {
    //If you call useForm() you get three functions which are handleSubmit, register and errors
    /* register is used to track changes in the form fields. register is also the way
       on how to connect the form fields to react-hook-form */
    // register, handleSubmit, errors is basically an api or method
    // register api - https://react-hook-form.com/api#register
    const { handleSubmit, register, errors } = useForm();

    //this function gets the data entered in the form
    const onSubmit = values => {
        console.log(values);
    };

    return (
        //calling the onsubmit function
        <form onSubmit={handleSubmit(onSubmit)}>
            <div>
                <input
                    name="username"
                    placeholder="Enter Username"
                    /* register is used to track changes in the form fields. register is also the way
                       on how to connect the form fields to react-hook-form */
                    /* register is an inbuilt method in react-hook-form which can take an 
                       object with values like validate, pattern, required. Check the documentation
                       for the complete list of values https://react-hook-form.com/api#register */
                    ref={register({
                        //if the username entered is admin it prints Nice Try!
                        validate: value => value !== "admin" || "Nice try!"
                    })}
                />
                {errors.username && <h1>{errors.username.message}</h1>}
            </div>
            <div>
                <input
                    name="email"
                    placeholder="Enter Email"
                    ref={register({
                        required: 'Required',
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                            message: "invalid email address"
                        }
                    })}
                />
                {errors.email && <h1>{errors.email.message}</h1>}


                <button type="submit">Submit</button>
            </div>
        </form >
    )
}

export default ReactHookForm