import React, { Component } from 'react';
import ApolloClient from "apollo-boost";
import { Mutation, ApolloProvider } from 'react-apollo';
import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "http://localhost:4000"
});


const ADD_EMPLOYER = gql`
    mutation add_employer{
        createEmployer(data: {
            name: "brow",
            email: "brow@gmail.com"
        }) {
            id
            name
            email
            createdAt
            updatedAt
            }
    }
`

class MutationExample extends Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <div className="App">
                    <Mutation mutation={ADD_EMPLOYER}>
                        {/* this is called as a render prop function */}
                        { /* data contains the result of the above query */}
                        {(createEmployer) => (
                            <button onClick={() => {
                                {/* When button is clicked call createEmployer function*/ }

                                createEmployer()
                            }}>ADD EMPLOYER</button>
                        )}
                    </Mutation>
                </div>
            </ApolloProvider>
        );
    }
}

export default MutationExample;


