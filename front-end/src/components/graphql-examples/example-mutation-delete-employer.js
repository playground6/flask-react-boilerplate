import React, { Component } from 'react';
import ApolloClient from "apollo-boost";
import { Mutation, ApolloProvider } from 'react-apollo';
import gql from "graphql-tag";

const client = new ApolloClient({
    uri: "http://localhost:4000"
});


const DELETE_EMPLOYER = gql`
  mutation deleteEmployer {
    deleteEmployer(where: {id: "ck8pjt4wxrxse0981qvmsmteu"}) {
        id
        name
        email
    }
}
`

class DeleteMutationExample extends Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <div className="App">
                    <Mutation mutation={DELETE_EMPLOYER}>
                        {/* this is called as a render prop function */}
                        { /* data contains the result of the above query */}
                        {(deleteEmployer) => (
                            <button onClick={() => {
                                {/* When button is clicked call createEmployer function*/ }

                                deleteEmployer()
                            }}>DELETE EMPLOYER</button>
                        )}
                    </Mutation>
                </div>
            </ApolloProvider>
        );
    }
}

export default DeleteMutationExample;


