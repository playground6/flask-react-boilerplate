# This is a temporary workaround till Poetry supports scripts, see
# https://github.com/sdispater/poetry/issues/241.
from subprocess import check_call


def format() -> None:
    check_call(
        ["api/"]
    )


def start() -> None:
    check_call(["python3", "api/run.py"])
